//USER JSON:

{
	"users": [

	{
		"userID": 1,
		"firstName": "John",
		"lastName": "Smith",
		"email": "jsmith@email.co",
		"password": "jSmitH123",
		"isAdmin": false,
		"mobileNumber": 639011011001,
		"order": {
			"orderID": 30601,
			"products": [{
				"productID": "MNG",
				"quantity": 5,
				"unitPrice": 20,
				"subTotal": 100
				},
				{
				"productID": "GRP",
				"quantity": 1,
				"unitPrice": 100,
				"subTotal": 100
				}
				],
			"status": "On delivery",
			"total": 200
		}
	},
	{
		"userID": 2,
		"firstName": "Anna",
		"lastName": "Smith",
		"email": "asmith@email.co",
		"password": "ASmitH123",
		"isAdmin": false,
		"mobileNumber": 639100101000,
		"order": {
			"orderID": 30701,
			"products": [{
				"productID": "MNG",
				"quantity": 5,
				"unitPrice": 20,
				"subTotal": 100
				},
				{
				"productID": "SSP",
				"quantity": 2,
				"unitPrice": 100,
				"subTotal": 200
				}
				],
			"status": "Still Packing",
			"total": 300
		}
	}

	]
}



//PRODUCTS JSON

{
	"products": [
		{
			name: "Mango",
			description: "Big and Sweet. Came from southern luzon area.",
			price: 20,
			stocks: 500,
			isActive: true,
			SKU: "MNG0227"
		},
		{
			name: "Grapes",
			description: "Locally grown grapes from the south.",
			price: 200,
			stocks: 100,
			isActive: true,
			SKU: GRP0219
		},
		{
			name: "Soursop",
			description: "Locally grown soursop from the north.",
			price: 100,
			stocks: 100,
			isActive: true,
			SKU: "SSP0301"
		},
		{
			name: "Cotton fruit",
			description: "Locally grown cotton fruit from the south.",
			price: 100,
			stocks: 0,
			isActive: false,
			SKU: "CNF0305"
		}
		]
}









//EXTRA
/*
let	users =  
`[
	{
		"firstName": "John",
		"lastName": "Smith",
		"email": "jsmith@email.co",
		"password": "jSmitH123",
		"isAdmin": false,
		"mobileNumber": 639011011001
	},

	{
		"firstName": "Anna",
		"lastName": "Smith",
		"email": "asmith@email.co",
		"password": "ASmitH123",
		"isAdmin": false,
		"mobileNumber": 639100101000
	}
]`

console.log(JSON.parse(users));

let orders = 
`[
	{ 
		"userID": "jsmith0306",
		"transactionDate": "March 6, 2023",
		"status": "Active",
		"total": 300
	},
	{
		"userID": "asmith0307",
		"transactionDate": "March 7, 2023",
		"status": "Active",
		"total": 100
	}
]`


let orderProducts = 
`[
	{
		"orderID": 1,
		"productID": "MNG",
		"quantity": 5,
		"price": 20,
		"subTotal": 100
	},
	{
		"orderID": 1,
		"productID": "SSP",
		"quantity": 2,
		"price": 100,
		"subTotal": 200
	},
	{
		"orderID": 2,
		"productID": "GRP",
		"quantity": 1,
		"price": 100,
		"subTotal": 100
	}
]`
*/
